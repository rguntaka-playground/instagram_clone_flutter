import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:instagram_clone_flutter/utils/colors.dart';
import 'package:instagram_clone_flutter/widgets/text_field_input.dart';

class SignupScreenWidget extends StatefulWidget {
  const SignupScreenWidget({Key? key}) : super(key: key);

  @override
  _SignupScreenWidgetState createState() => _SignupScreenWidgetState();
}

class _SignupScreenWidgetState extends State<SignupScreenWidget> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _bioController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _bioController.dispose();
    _userNameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Container(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      width: double.infinity,
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Flexible(
          child: Container(),
          flex: 2,
        ),
        // logo on login screen
        SvgPicture.asset('assets/ic_instagram_text.svg',
            color: primaryColor, height: 64),
        const SizedBox(height: 64),
        Stack(
          children: [
            CircleAvatar(
              radius: 64,
              backgroundImage: NetworkImage(
                  'https://images.unsplash.com/photo-1642486488748-c7d180189e7e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=465&q=80'),
            )
          ],
        ),
        TextFieldInput(
            textEditingController: _userNameController,
            hintText: "Enter your username",
            textInputType: TextInputType.text),
        const SizedBox(height: 24),
        TextFieldInput(
            textEditingController: _emailController,
            hintText: "Enter your Email",
            textInputType: TextInputType.emailAddress),
        const SizedBox(height: 24),
        TextFieldInput(
          textEditingController: _passwordController,
          hintText: "Enter your Password",
          textInputType: TextInputType.text,
          isPass: true,
        ),
        const SizedBox(height: 24),
        TextFieldInput(
            textEditingController: _bioController,
            hintText: "Enter your bio",
            textInputType: TextInputType.text),
        const SizedBox(height: 24),
        InkWell(
          onTap: () {},
          child: Container(
            child: const Text('Log In'),
            width: double.infinity,
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 12),
            decoration: const ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4))),
                color: blueColor),
          ),
        ),
        const SizedBox(height: 12),
        Flexible(
          child: Container(),
          flex: 2,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: const Text(
                "Don't have an account? ",
              ),
              padding: const EdgeInsets.symmetric(vertical: 8),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: const Text(
                  "Sign Up",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                padding: const EdgeInsets.symmetric(vertical: 8),
              ),
            )
          ],
        )
      ]),
    )));
  }
}
